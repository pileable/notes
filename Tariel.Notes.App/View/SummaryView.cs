﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tariel.Notes.App.ViewModel;
using Tariel.Notes.Services;
using Tariel.Notes.ViewModel;

namespace Tariel.Notes.App.View
{
  public partial class SummaryView : UserControl
  {
    private readonly ViewModel.ViewModel _viewModel;
    public SummaryView()
    {
      InitializeComponent();
      try
      {
        _viewModel = ViewModel.ViewModel.Instance;
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
    }

    private void OnLoad(object sender, EventArgs e)
    {
      try
      {
        Bind();
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }

    }

    private void Bind()
    {
      this.summaryBindingSource.DataSource = _viewModel.Items;
      _viewModel.Items.ListChanged += ItemsOnListChanged;
    }
    


    private void ItemsOnListChanged(object sender, ListChangedEventArgs listChangedEventArgs)
    {
    }

    private void OnCellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      try
      {
        var row = summaryDataGridView.SelectedRows[0];
        if (row != null)
        {
          var obj = row.DataBoundItem as NoteViewModel;
          if (obj != null)
            _viewModel.FireItemEditingStarted(this, new ViewModelEventArgs(obj));
        }
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }

    public void Remove()
    {
      try
      {
        var row = summaryDataGridView.SelectedRows[0];
        if (row != null)
        {
          var obj = row.DataBoundItem as NoteViewModel;
          if (obj != null)
            _viewModel.FireItemDeletingStarted(this, new ViewModelEventArgs(obj));
        }
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }
  }
}
