﻿namespace Tariel.Notes.App.View
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.mainPanel = new System.Windows.Forms.TableLayoutPanel();
      this.buttonsPanel = new System.Windows.Forms.Panel();
      this.deleteButton = new System.Windows.Forms.Button();
      this.syncButton = new System.Windows.Forms.Button();
      this.newButton = new System.Windows.Forms.Button();
      this.mainPanel.SuspendLayout();
      this.buttonsPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // mainPanel
      // 
      this.mainPanel.ColumnCount = 2;
      this.mainPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
      this.mainPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.mainPanel.Controls.Add(this.buttonsPanel, 1, 0);
      this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainPanel.Location = new System.Drawing.Point(0, 0);
      this.mainPanel.Name = "mainPanel";
      this.mainPanel.RowCount = 1;
      this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainPanel.Size = new System.Drawing.Size(669, 342);
      this.mainPanel.TabIndex = 0;
      // 
      // buttonsPanel
      // 
      this.buttonsPanel.Controls.Add(this.deleteButton);
      this.buttonsPanel.Controls.Add(this.syncButton);
      this.buttonsPanel.Controls.Add(this.newButton);
      this.buttonsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.buttonsPanel.Location = new System.Drawing.Point(538, 3);
      this.buttonsPanel.Name = "buttonsPanel";
      this.buttonsPanel.Size = new System.Drawing.Size(128, 336);
      this.buttonsPanel.TabIndex = 1;
      // 
      // deleteButton
      // 
      this.deleteButton.Location = new System.Drawing.Point(3, 61);
      this.deleteButton.Name = "deleteButton";
      this.deleteButton.Size = new System.Drawing.Size(75, 23);
      this.deleteButton.TabIndex = 3;
      this.deleteButton.Text = "Delete";
      this.deleteButton.UseVisualStyleBackColor = true;
      this.deleteButton.Click += new System.EventHandler(this.OnDeleteClick);
      // 
      // syncButton
      // 
      this.syncButton.Location = new System.Drawing.Point(3, 3);
      this.syncButton.Name = "syncButton";
      this.syncButton.Size = new System.Drawing.Size(75, 23);
      this.syncButton.TabIndex = 1;
      this.syncButton.Text = "Sync";
      this.syncButton.UseVisualStyleBackColor = true;
      this.syncButton.Click += new System.EventHandler(this.OnSyncButtonClick);
      // 
      // newButton
      // 
      this.newButton.Location = new System.Drawing.Point(3, 32);
      this.newButton.Name = "newButton";
      this.newButton.Size = new System.Drawing.Size(75, 23);
      this.newButton.TabIndex = 0;
      this.newButton.Text = "New";
      this.newButton.UseVisualStyleBackColor = true;
      this.newButton.Click += new System.EventHandler(this.OnNewButtonClick);
      // 
      // Main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.ClientSize = new System.Drawing.Size(669, 342);
      this.Controls.Add(this.mainPanel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "Main";
      this.Text = "My Notes";
      this.Load += new System.EventHandler(this.OnLoad);
      this.mainPanel.ResumeLayout(false);
      this.buttonsPanel.ResumeLayout(false);
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainPanel;
        private System.Windows.Forms.Panel buttonsPanel;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Button syncButton;
    private System.Windows.Forms.Button deleteButton;
  }
}