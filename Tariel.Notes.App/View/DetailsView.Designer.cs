﻿namespace Tariel.Notes.App.View
{
    partial class DetailsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      this.detailsTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.headerLabel = new System.Windows.Forms.Label();
      this.headerTextBox = new System.Windows.Forms.TextBox();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.txtLabel = new System.Windows.Forms.Label();
      this.noteTextBox = new System.Windows.Forms.TextBox();
      this.buttonsTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
      this.saveButton = new System.Windows.Forms.Button();
      this.cancelButton = new System.Windows.Forms.Button();
      this.notesErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
      this.detailsTableLayoutPanel.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.buttonsTableLayoutPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.notesErrorProvider)).BeginInit();
      this.SuspendLayout();
      // 
      // detailsTableLayoutPanel
      // 
      this.detailsTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.detailsTableLayoutPanel.ColumnCount = 1;
      this.detailsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.detailsTableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 1);
      this.detailsTableLayoutPanel.Controls.Add(this.tableLayoutPanel2, 0, 2);
      this.detailsTableLayoutPanel.Controls.Add(this.buttonsTableLayoutPanel, 0, 3);
      this.detailsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.detailsTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
      this.detailsTableLayoutPanel.Name = "detailsTableLayoutPanel";
      this.detailsTableLayoutPanel.RowCount = 5;
      this.detailsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.detailsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.96703F));
      this.detailsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.95604F));
      this.detailsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.07692F));
      this.detailsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.detailsTableLayoutPanel.Size = new System.Drawing.Size(361, 295);
      this.detailsTableLayoutPanel.TabIndex = 0;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Controls.Add(this.headerLabel, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.headerTextBox, 0, 1);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 23);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(355, 78);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // headerLabel
      // 
      this.headerLabel.AutoSize = true;
      this.headerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.headerLabel.Location = new System.Drawing.Point(3, 0);
      this.headerLabel.Name = "headerLabel";
      this.headerLabel.Size = new System.Drawing.Size(48, 13);
      this.headerLabel.TabIndex = 0;
      this.headerLabel.Text = "Header";
      // 
      // headerTextBox
      // 
      this.headerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.headerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.headerTextBox.Location = new System.Drawing.Point(3, 18);
      this.headerTextBox.MaxLength = 249;
      this.headerTextBox.Multiline = true;
      this.headerTextBox.Name = "headerTextBox";
      this.headerTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.headerTextBox.Size = new System.Drawing.Size(322, 57);
      this.headerTextBox.TabIndex = 1;
      this.headerTextBox.TextChanged += new System.EventHandler(this.OnHeaderTextBoxTextChanged);
      this.headerTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.OnHeaderTextBoxValidating);
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 1;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel2.Controls.Add(this.txtLabel, 0, 0);
      this.tableLayoutPanel2.Controls.Add(this.noteTextBox, 0, 1);
      this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 107);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 2;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(355, 106);
      this.tableLayoutPanel2.TabIndex = 1;
      // 
      // txtLabel
      // 
      this.txtLabel.AutoSize = true;
      this.txtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtLabel.Location = new System.Drawing.Point(3, 0);
      this.txtLabel.Name = "txtLabel";
      this.txtLabel.Size = new System.Drawing.Size(34, 13);
      this.txtLabel.TabIndex = 0;
      this.txtLabel.Text = "Note";
      // 
      // noteTextBox
      // 
      this.noteTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.noteTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.noteTextBox.Location = new System.Drawing.Point(3, 24);
      this.noteTextBox.MaxLength = 999;
      this.noteTextBox.Multiline = true;
      this.noteTextBox.Name = "noteTextBox";
      this.noteTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.noteTextBox.Size = new System.Drawing.Size(322, 79);
      this.noteTextBox.TabIndex = 1;
      // 
      // buttonsTableLayoutPanel
      // 
      this.buttonsTableLayoutPanel.ColumnCount = 4;
      this.buttonsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.buttonsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.buttonsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
      this.buttonsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
      this.buttonsTableLayoutPanel.Controls.Add(this.saveButton, 0, 1);
      this.buttonsTableLayoutPanel.Controls.Add(this.cancelButton, 1, 1);
      this.buttonsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.buttonsTableLayoutPanel.Location = new System.Drawing.Point(3, 219);
      this.buttonsTableLayoutPanel.Name = "buttonsTableLayoutPanel";
      this.buttonsTableLayoutPanel.RowCount = 3;
      this.buttonsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.buttonsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.buttonsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.buttonsTableLayoutPanel.Size = new System.Drawing.Size(355, 52);
      this.buttonsTableLayoutPanel.TabIndex = 2;
      // 
      // saveButton
      // 
      this.saveButton.Enabled = false;
      this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.saveButton.Location = new System.Drawing.Point(3, 3);
      this.saveButton.Name = "saveButton";
      this.saveButton.Size = new System.Drawing.Size(65, 23);
      this.saveButton.TabIndex = 0;
      this.saveButton.Text = "Save";
      this.saveButton.UseVisualStyleBackColor = true;
      this.saveButton.Click += new System.EventHandler(this.OnSaveButtonClick);
      // 
      // cancelButton
      // 
      this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cancelButton.Location = new System.Drawing.Point(74, 3);
      this.cancelButton.Name = "cancelButton";
      this.cancelButton.Size = new System.Drawing.Size(65, 23);
      this.cancelButton.TabIndex = 1;
      this.cancelButton.Text = "Cancel";
      this.cancelButton.UseVisualStyleBackColor = true;
      this.cancelButton.Click += new System.EventHandler(this.OnCancelButtonClick);
      // 
      // notesErrorProvider
      // 
      this.notesErrorProvider.ContainerControl = this;
      // 
      // DetailsView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.detailsTableLayoutPanel);
      this.Name = "DetailsView";
      this.Size = new System.Drawing.Size(361, 295);
      this.Load += new System.EventHandler(this.OnLoad);
      this.detailsTableLayoutPanel.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel2.PerformLayout();
      this.buttonsTableLayoutPanel.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.notesErrorProvider)).EndInit();
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel detailsTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label headerLabel;
        private System.Windows.Forms.TextBox headerTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label txtLabel;
        private System.Windows.Forms.TextBox noteTextBox;
    private System.Windows.Forms.TableLayoutPanel buttonsTableLayoutPanel;
    private System.Windows.Forms.Button saveButton;
    private System.Windows.Forms.Button cancelButton;
    private System.Windows.Forms.ErrorProvider notesErrorProvider;
  }
}
