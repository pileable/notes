﻿using Tariel.Notes.ViewModel;

namespace Tariel.Notes.App.View
{
    partial class SummaryView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      this.summaryTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
      this.summaryDataGridView = new System.Windows.Forms.DataGridView();
      this.summaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.headerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.txtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.summaryTableLayoutPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.summaryDataGridView)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.summaryBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // summaryTableLayoutPanel
      // 
      this.summaryTableLayoutPanel.ColumnCount = 1;
      this.summaryTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.summaryTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.summaryTableLayoutPanel.Controls.Add(this.summaryDataGridView, 0, 1);
      this.summaryTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.summaryTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
      this.summaryTableLayoutPanel.Name = "summaryTableLayoutPanel";
      this.summaryTableLayoutPanel.RowCount = 3;
      this.summaryTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.summaryTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.summaryTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.summaryTableLayoutPanel.Size = new System.Drawing.Size(402, 276);
      this.summaryTableLayoutPanel.TabIndex = 0;
      // 
      // summaryDataGridView
      // 
      this.summaryDataGridView.AllowUserToAddRows = false;
      this.summaryDataGridView.AllowUserToDeleteRows = false;
      this.summaryDataGridView.AllowUserToResizeRows = false;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
      this.summaryDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
      this.summaryDataGridView.AutoGenerateColumns = false;
      this.summaryDataGridView.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.summaryDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
      this.summaryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.summaryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.headerDataGridViewTextBoxColumn,
            this.txtDataGridViewTextBoxColumn});
      this.summaryDataGridView.DataSource = this.summaryBindingSource;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.summaryDataGridView.DefaultCellStyle = dataGridViewCellStyle3;
      this.summaryDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.summaryDataGridView.Location = new System.Drawing.Point(3, 23);
      this.summaryDataGridView.MultiSelect = false;
      this.summaryDataGridView.Name = "summaryDataGridView";
      this.summaryDataGridView.ReadOnly = true;
      this.summaryDataGridView.RowHeadersVisible = false;
      this.summaryDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.summaryDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.summaryDataGridView.Size = new System.Drawing.Size(396, 230);
      this.summaryDataGridView.TabIndex = 1;
      this.summaryDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.OnCellMouseDoubleClick);
      // 
      // summaryBindingSource
      // 
      this.summaryBindingSource.DataSource = typeof(Tariel.Notes.ViewModel.NoteViewModel);
      // 
      // headerDataGridViewTextBoxColumn
      // 
      this.headerDataGridViewTextBoxColumn.DataPropertyName = "Header";
      this.headerDataGridViewTextBoxColumn.HeaderText = "Header";
      this.headerDataGridViewTextBoxColumn.Name = "headerDataGridViewTextBoxColumn";
      this.headerDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // txtDataGridViewTextBoxColumn
      // 
      this.txtDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.txtDataGridViewTextBoxColumn.DataPropertyName = "Txt";
      this.txtDataGridViewTextBoxColumn.HeaderText = "Text";
      this.txtDataGridViewTextBoxColumn.MinimumWidth = 100;
      this.txtDataGridViewTextBoxColumn.Name = "txtDataGridViewTextBoxColumn";
      this.txtDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // SummaryView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.summaryTableLayoutPanel);
      this.Name = "SummaryView";
      this.Size = new System.Drawing.Size(402, 276);
      this.Load += new System.EventHandler(this.OnLoad);
      this.summaryTableLayoutPanel.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.summaryDataGridView)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.summaryBindingSource)).EndInit();
      this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource summaryBindingSource;
    private System.Windows.Forms.TableLayoutPanel summaryTableLayoutPanel;
    internal System.Windows.Forms.DataGridView summaryDataGridView;
    private System.Windows.Forms.DataGridViewTextBoxColumn headerDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn txtDataGridViewTextBoxColumn;
  }
}
