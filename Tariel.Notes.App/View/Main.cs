﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Tariel.Notes.App.ViewModel;
using Tariel.Notes.Services;
using Tariel.Notes.ViewModel;

namespace Tariel.Notes.App.View
{
  public partial class Main : Form
  {
    private readonly SummaryView _summaryView;
    private readonly DetailsView _detailsView;
    private readonly ViewModel.ViewModel _viewModel;
    public Main()
    {
      InitializeComponent();
      try
      {
        ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
        SimpleIoc.Default.Register<IDatabaseConnectionFactory<NotesDataContext>>(() => new SqlCeDatabaseConnectionFactory<NotesDataContext>(Path.Combine(Directory.GetCurrentDirectory(), @"Tariel.Notes.sdf")), true);
        SimpleIoc.Default.Register<ILocalRepository>(() => new CompactDbRepository(new NotesDataContext()));
        SimpleIoc.Default.Register<IRemoteRepository>(() => new SimpleRemoteRepository());
        var syncService = new Simpleton();
        SimpleIoc.Default.Register<ISyncService>(() => syncService);

        _summaryView = new SummaryView();
        _detailsView = new DetailsView();
        _viewModel = ViewModel.ViewModel.Instance;
        _viewModel.ItemAddingStarted += ViewModelOnItemAddingStarted;
        _viewModel.ItemEditingStarted += ViewModelOnItemEditingStarted;
        _viewModel.ItemEditingCancelled += ViewModelOnItemEditingCancelled;
        _viewModel.ItemAddingCompleted += ViewModelOnItemAddingCompleted;
        _viewModel.ItemEditingCompleted += ViewModelOnItemEditingCompleted;
        _viewModel.ItemDeletingStarted += ViewModelOnItemDeletingStarted;
        syncService.SyncCompleted += SyncServiceOnSyncCompleted;
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
        throw;
      }
    }

    private void SyncServiceOnSyncCompleted(object sender, EventArgs eventArgs)
    {
      MessageBox.Show("Synchronization completed!");
    }

    private void ViewModelOnItemDeletingStarted(object sender, ViewModelEventArgs viewModelEventArgs)
    {
      try
      {
        _viewModel.Delete(viewModelEventArgs.NoteViewModel);
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
    }

    private void ViewModelOnItemEditingCompleted(object sender, ViewModelEventArgs viewModelEventArgs)
    {
      _viewModel.Adjust(viewModelEventArgs.NoteViewModel);
      Swap();
    }

    private void ViewModelOnItemAddingCompleted(object sender, ViewModelEventArgs viewModelEventArgs)
    {
      _viewModel.Add(viewModelEventArgs.NoteViewModel);
      Swap();
    }

    private void ViewModelOnItemEditingCancelled(object sender, ViewModelEventArgs viewModelEventArgs)
    {
      Swap();
    }

    private void ViewModelOnItemEditingStarted(object sender, ViewModelEventArgs viewModelEventArgs)
    {
      Swap(false, viewModelEventArgs.NoteViewModel);
    }

    private void ViewModelOnItemAddingStarted(object sender, ViewModelEventArgs viewModelEventArgs)
    {
      Swap(false);
    }

    private void OnLoad(object sender, EventArgs e)
    {
      Swap();
    }

    private void Swap(bool showSummary = true, NoteViewModel noteViewModel = null)
    {
      try
      {
        if (showSummary)
        {
          this.mainPanel.Controls.Remove(_detailsView);
          _summaryView.Dock = DockStyle.Fill;
          this.mainPanel.Controls.Add(_summaryView, 0, 0);
          deleteButton.Visible = true;
          newButton.Visible = true;
        }
        else
        {
          this.mainPanel.Controls.Remove(_summaryView);
          _detailsView.Dock = DockStyle.Fill;
          this.mainPanel.Controls.Add(_detailsView, 0, 0);
          deleteButton.Visible = false;
          newButton.Visible = false;
        }
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }

    private void OnNewButtonClick(object sender, EventArgs e)
    {
      _viewModel.FireItemAddingStarted(null, null);
    }

    private void OnSyncButtonClick(object sender, EventArgs e)
    {
      ISyncService syncService = ServiceLocator.Current.GetAllInstances<ISyncService>().FirstOrDefault();
      if (syncService != null && !syncService.IsInProcess)
        syncService.Sync();
    }

    private void OnDeleteClick(object sender, EventArgs e)
    {
      try
      {
        _summaryView.Remove();
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }
  }
}
