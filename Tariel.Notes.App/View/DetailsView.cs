﻿using System;
using System.Windows.Forms;
using Tariel.Notes.App.ViewModel;
using Tariel.Notes.Model;
using Tariel.Notes.Services;
using Tariel.Notes.ViewModel;

namespace Tariel.Notes.App.View
{
  public partial class DetailsView : UserControl
  {
    private readonly ViewModel.ViewModel _viewModel;

    public DetailsView()
    {
      InitializeComponent();
      try
      {
        _viewModel = ViewModel.ViewModel.Instance;
        _viewModel.ItemEditingStarted += ViewModelOnItemEditingStarted;
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
        throw;
      }
    }

    private void ViewModelOnItemEditingStarted(object sender, ViewModelEventArgs e)
    {
      if (e == null) throw new ArgumentNullException(nameof(e));
      if (e.NoteViewModel != null)
      {
        NoteViewModel = e.NoteViewModel;
        this.headerTextBox.Text = NoteViewModel.Header;
        this.noteTextBox.Text = NoteViewModel.Txt;
      }
    }

    public NoteViewModel NoteViewModel { get; set; }

    private void OnLoad(object sender, EventArgs e)
    {

    }

    private void Clear()
    {
      this.headerTextBox.Text = null;
      this.noteTextBox.Text = null;
      NoteViewModel = null;
      notesErrorProvider.SetError(headerTextBox, null);
    }

    private void OnCancelButtonClick(object sender, EventArgs e)
    {
      _viewModel.FireItemEditingCancelled(null, null);
      Clear();
    }

    private void OnSaveButtonClick(object sender, EventArgs e)
    {
      if (NoteViewModel == null)
      {
        Note note = new Note
        {
          Header = headerTextBox.Text,
          Txt = noteTextBox.Text
        };
        _viewModel.FireItemAddingCompleted(null, new ViewModelEventArgs(new NoteViewModel(note)));
      }
      else
      {
        NoteViewModel.Header = headerTextBox.Text;
        NoteViewModel.Txt = noteTextBox.Text;

        _viewModel.FireItemEditingCompleted(null, new ViewModelEventArgs(NoteViewModel));
      }
      Clear();
    }

    private void OnHeaderTextBoxValidating(object sender, System.ComponentModel.CancelEventArgs e)
    {
      try
      {
        ValidateTextBox();
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }

    private void ValidateTextBox()
    {
      if (string.IsNullOrEmpty(headerTextBox.Text))
      {
        notesErrorProvider.SetError(headerTextBox, "Header cannot be empty!");
      }
      else
      {
        notesErrorProvider.SetError(headerTextBox, null);
      }
    }

    private void OnHeaderTextBoxTextChanged(object sender, EventArgs e)
    {
      saveButton.Enabled = !string.IsNullOrEmpty(headerTextBox.Text);
    }
  }
}
