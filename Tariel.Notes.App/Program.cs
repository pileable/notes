﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Tariel.Notes.App.View;

namespace Tariel.Notes.App
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }
    }
}
