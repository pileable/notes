﻿using Tariel.Notes.Services;

namespace Tariel.Notes.App
{
  public class NotesSortableBindingList<T> : SortableBindingList<T>
  {
    public void ForceSort()
    {
      if (SortPropertyCore != null)
        ApplySortCore(SortPropertyCore, SortDirectionCore);
    }
  }
}
