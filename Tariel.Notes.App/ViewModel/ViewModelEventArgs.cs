using System;
using Tariel.Notes.ViewModel;

namespace Tariel.Notes.App.ViewModel
{
  public class ViewModelEventArgs : EventArgs
  {
    public NoteViewModel NoteViewModel { get; set; }

    public ViewModelEventArgs(NoteViewModel note)
    {
      if (note == null)
        throw new ArgumentNullException(nameof(note));
      NoteViewModel = note;
    }
  }
}
