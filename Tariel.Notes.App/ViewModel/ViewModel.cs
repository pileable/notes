using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Tariel.Notes.Model;
using Tariel.Notes.Services;
using Tariel.Notes.ViewModel;

namespace Tariel.Notes.App.ViewModel
{
  public class ViewModel
  {

    private NotesSortableBindingList<NoteViewModel> _notes;
    private ILocalRepository _localRepository;
    private ISyncService _syncService;

    private static readonly Lazy<ViewModel> Singleton = new Lazy<ViewModel>(() =>
    {
      ILocalRepository localRepository = ServiceLocator.Current.GetAllInstances<ILocalRepository>().FirstOrDefault();

      if (localRepository == null)
        throw new InvalidOperationException("ServiceLocator does not contain any ILocalRepository object.");
      ViewModel instance = new ViewModel();
      instance._syncService = ServiceLocator.Current.GetAllInstances<ISyncService>().FirstOrDefault();
      instance._localRepository = localRepository;
      instance.Init();
      return instance;
    });

    public static ViewModel Instance
    {
      get
      {
        try
        {
          return Singleton.Value;
        }
        catch (Exception ex)
        {
          Log.Instance.Err(ex);
          throw new InvalidOperationException("Cannot obtain ViewModel.Instance. " + ex.Message);
        }
      }
    }

    private ViewModel()
    {
      _notes = new NotesSortableBindingList<NoteViewModel>();
    }

    private void Init()
    {
      this._localRepository.EntryAdded += LocalRepositoryOnEntryAdded;
      this._localRepository.EntryDeleted += LocalRepositoryOnEntryDeleted;
      if (this._syncService != null)
      {
        this._syncService.SyncCompleted += SyncServiceOnSyncCompleted;
      }
      LoadItems();
    }

    private void SyncServiceOnSyncCompleted(object sender, EventArgs eventArgs)
    {
      try
      {
        _notes.Clear();
        LoadItems();
        _notes.ForceSort();
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }

    private void LocalRepositoryOnEntryDeleted(object sender, ModelEventArgs e)
    {
      try
      {
        _notes.ForceSort();
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }

    private void LoadItems()
    {
      var collection = this._localRepository.Notes;
      if (collection != null)
      {
        foreach (var note in collection)
        {
          _notes.Add(new NoteViewModel(note));
        }
      }
    }

    private void LocalRepositoryOnEntryAdded(object sender, ModelEventArgs e)
    {
      try
      {
        _notes.Add(new NoteViewModel(e.Note));
        _notes.ForceSort();
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }

    public void Add(NoteViewModel note)
    {
      if (note == null) throw new ArgumentException(nameof(note));
      try
      {
        this._localRepository.Add(note.Model);
        this._localRepository.SaveChanges();
        CheckMaxEntriesCount();
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
    }

    private void CheckMaxEntriesCount()
    {
      try
      {
        if (_notes.Count >= Constants.MAX_NOTES_COUNT)
        {
          var min = (from vm in _notes orderby vm.Model.Modified ascending select vm).FirstOrDefault();
          if (min != null)
            Delete(min);
        }
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
    }

    public void Adjust(NoteViewModel note)
    {
      if (note == null) throw new ArgumentException(nameof(note));
      try
      {
        this._localRepository.Update(note.Model);
        this._localRepository.SaveChanges();
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
    }

    public void Delete(NoteViewModel note)
    {
      if (note == null) throw new ArgumentException(nameof(note));
      try
      {
        this._localRepository.Delete(note.Model);
        this._localRepository.SaveChanges();
        _notes.Remove(note);
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
    }

    public SortableBindingList<NoteViewModel> Items
    {
      get { return _notes; }
    }

    public event EventHandler<ViewModelEventArgs> ItemAddingStarted;
    public event EventHandler<ViewModelEventArgs> ItemEditingStarted;
    public event EventHandler<ViewModelEventArgs> ItemEditingCancelled;
    public event EventHandler<ViewModelEventArgs> ItemEditingCompleted;
    public event EventHandler<ViewModelEventArgs> ItemAddingCompleted;
    public event EventHandler<ViewModelEventArgs> ItemDeletingCompleted;
    public event EventHandler<ViewModelEventArgs> ItemDeletingStarted;

    public void FireItemDeletingCompleted(object sender, ViewModelEventArgs e)
    {
      if (ItemDeletingCompleted != null)
        ItemDeletingCompleted(sender, e);
    }

    public void FireItemDeletingStarted(object sender, ViewModelEventArgs e)
    {
      if (ItemDeletingStarted != null)
        ItemDeletingStarted(sender, e);
    }

    public void FireItemEditingCancelled(object sender, ViewModelEventArgs e)
    {
      if (ItemEditingCancelled != null)
        ItemEditingCancelled(sender, e);
    }
    public void FireItemEditingCompleted(object sender, ViewModelEventArgs e)
    {
      if (ItemEditingCompleted != null)
        ItemEditingCompleted(sender, e);
    }
    public void FireItemAddingCompleted(object sender, ViewModelEventArgs e)
    {
      if (ItemAddingCompleted != null)
        ItemAddingCompleted(sender, e);
    }
   
    public void FireItemAddingStarted(object sender, ViewModelEventArgs e)
    {
      if (ItemAddingStarted != null)
        ItemAddingStarted(sender, e);
    }

    public void FireItemEditingStarted(object sender, ViewModelEventArgs e)
    {
      if (ItemEditingStarted != null)
        ItemEditingStarted(sender, e);
    }
  }
}