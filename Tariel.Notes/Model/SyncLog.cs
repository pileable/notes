﻿using System;
using System.ComponentModel;

namespace Tariel.Notes.Model
{
  public class SyncLog : INotifyPropertyChanged
  {
    public Guid Id { get; set; }
    public DateTime Started { get; set; }
    public DateTime? Finished { get; set; }

    public SyncLog()
    {
      Id = Guid.NewGuid();
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      var handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
