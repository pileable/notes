﻿using System;
using System.ComponentModel;

namespace Tariel.Notes.Model
{
  public class Note : INotifyPropertyChanged
  {
    private string _header;
    private string _txt;
    private DateTime _modified;
    public Guid Id { get; set; }

    public string Header
    {
      get { return _header; }
      set
      {
        if (value.Equals(_header)) return;
        _header = value;
        Modified = DateTime.UtcNow;
        OnPropertyChanged(nameof(Header));
      }
    }

    public string Txt
    {
      get { return _txt; }
      set
      {
        if (value.Equals(_txt)) return;
        _txt = value;
        Modified = DateTime.UtcNow;
        OnPropertyChanged(nameof(Txt));
      }
    }

    public DateTime Modified
    {
      get { return _modified; }
      set
      {
        if (value.Equals(_modified)) return;
        _modified = value;
        OnPropertyChanged(nameof(Modified));
      }
    }

    [Localizable(false)]
    public override string ToString()
    {
      return string.Format("{{Header: {0}, Text: {1}}}", Header, Txt);
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      var handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
