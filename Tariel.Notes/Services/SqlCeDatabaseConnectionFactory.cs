﻿using System;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServerCompact;
using System.Data.SqlServerCe;

namespace Tariel.Notes.Services
{
  public class SqlCeDatabaseConnectionFactory<TContext> : IDatabaseConnectionFactory<TContext>
  {
    public SqlCeDatabaseConnectionFactory(string dataSource)
    {
      if (dataSource == null) throw new ArgumentNullException(nameof(dataSource));

      DataSource = dataSource;
    }
    public string DataSource { get; private set; }
    public DbConnection CreateConnection()
    {
      var factory = new SqlCeConnectionFactory(SqlCeProviderServices.ProviderInvariantName);
      var connectionString = new SqlCeConnectionStringBuilder { DataSource = DataSource, MaxDatabaseSize = 16 }.ConnectionString;

      return factory.CreateConnection(connectionString);
    }
  }
}
