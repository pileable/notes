﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Tariel.Notes.Model;

namespace Tariel.Notes.Services
{
  public class SimpleRemoteRepository : IRemoteRepository
  {
    public IDictionary<Guid, Note> Load()
    {
      IDictionary<Guid, Note> ret = new Dictionary<Guid, Note>(32);

      try
      {
        HttpWebRequest request = WebRequest.Create(Constants.URI) as HttpWebRequest;
        if (request != null)
        {
          request.Method = "GET";
          request.ContentType = "application/json";

          using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
          {
            if (response.StatusCode != HttpStatusCode.OK)
              throw new InvalidOperationException(string.Format("Server error (HTTP {0}: {1}).", response.StatusCode, response.StatusDescription));
            Stream stream1 = response.GetResponseStream();
            if (stream1 != null)
            {
              using (StreamReader sr = new StreamReader(stream1))
              {
                string strsb = sr.ReadToEnd();
                var notes = JsonConvert.DeserializeObject(strsb, typeof(ICollection<Note>)) as ICollection<Note>;
                if (notes != null)
                {
                  foreach (var note in notes)
                  {
                    ret.Add(note.Id, note);
                  }
                }
              }
            }
            else
            {
              Log.Instance.Warn("No data came from the server");
            }
          }
        }
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
      
      //string filename = Path.Combine(Directory.GetCurrentDirectory(), @"repo.json");
      //if (File.Exists(filename))
      //{
      //  ICollection<Note> notes = JsonConvert.DeserializeObject<ICollection<Note>>(File.ReadAllText(filename));
      //  if (notes != null)
      //  {
      //    foreach (var note in notes)
      //    {
      //      ret.Add(note.Id, note);
      //    }
      //  }
      //}
      return ret;
    }

    public void Save(IOrderedQueryable<Note> data)
    {
      try
      {
        HttpWebRequest request = WebRequest.Create(Constants.URI) as HttpWebRequest;
        if (request != null)
        {
          request.Method = "PUT";
          request.ContentType = "application/json";
          if (data.Any())
          {
            string sb = JsonConvert.SerializeObject(data);
            Byte[] bt = Encoding.UTF8.GetBytes(sb);
            Stream st = request.GetRequestStream();
            st.Write(bt, 0, bt.Length);
            st.Close();
          }
          else
          {
            request.ContentLength = 0;
          }

          using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
          {
            if (response.StatusCode != HttpStatusCode.OK)
              throw new InvalidOperationException(string.Format("Server error (HTTP {0}: {1}).", response.StatusCode,
                response.StatusDescription));
          }
        }
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }

      //string filename = Path.Combine(Directory.GetCurrentDirectory(), @"repo.json");
      //using (StreamWriter file = File.CreateText(filename))
      //{
      //  JsonSerializer serializer = new JsonSerializer();
      //  serializer.Serialize(file, data);
      //}
    }
  }
}
