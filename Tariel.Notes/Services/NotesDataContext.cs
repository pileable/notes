﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using Tariel.Notes.Model;

namespace Tariel.Notes.Services
{
  public class NotesDataContext : DbContext
  {
    public NotesDataContext() : this(GetDbConnection())
    {
    }
    public NotesDataContext(DbConnection dbConnection) : base(dbConnection, true)
    {
      Configuration.LazyLoadingEnabled = false;
      Configuration.ProxyCreationEnabled = false;
    }

    public static void WarmUp()
    {
      try
      {
        Log.Instance.Info("Context warm up");

        using (var dataContext = new NotesDataContext())
        {
          int count = dataContext.Notes.Count();
          Log.Instance.Info("Notes count {0}.", count);
        }
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      modelBuilder.Configurations.AddFromAssembly(typeof(NotesDataContext).Assembly);
    }
    public static DbConnection GetDbConnection()
    {
      try
      {
        var connectionFactory = GetDatabseConnectionFactory();

        var databseConnectionFactory = connectionFactory ?? new SqlCeDatabaseConnectionFactory<NotesDataContext>(@"|DataDirectory|\tariel.notes.sdf");
        var connectionString = databseConnectionFactory.CreateConnection();

        return connectionString;
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
        throw;
      }
    }

    private static IDatabaseConnectionFactory<NotesDataContext> GetDatabseConnectionFactory()
    {
      if (ServiceLocator.IsLocationProviderSet)
        return ServiceLocator.Current.GetAllInstances<IDatabaseConnectionFactory<NotesDataContext>>().FirstOrDefault();

      return null;
    }

    public DbSet<Note> Notes { get; set; }
    public DbSet<SyncLog> SyncLogs { get; set; }
  }
}
