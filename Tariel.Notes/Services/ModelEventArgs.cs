using System;
using Tariel.Notes.Model;

namespace Tariel.Notes.Services
{
  public class ModelEventArgs : EventArgs
  {
    public Note Note { get; set; }

    public SyncLog SyncLog { get; set; }

    public ModelEventArgs(Note note)
    {
      if (note == null)
        throw new ArgumentNullException(nameof(note));
      Note = note;
    }

    public ModelEventArgs(SyncLog syncLog)
    {
      if (syncLog == null)
        throw new ArgumentNullException(nameof(syncLog));
      SyncLog = syncLog;
    }
  }
}
