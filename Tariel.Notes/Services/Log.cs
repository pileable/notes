﻿using System;
using System.ComponentModel;
using NLog;

namespace Tariel.Notes.Services
{
  [Localizable(false)]
  public class Log
  {
    private Logger _logger;
    private static readonly Lazy<Log> Singleton = new Lazy<Log>(() =>
    {
      Log instance = new Log();
      instance._logger = LogManager.GetCurrentClassLogger();
      return instance;
    });

    public static Log Instance
    {
      get
      {
        try
        {
          return Singleton.Value;
        }
        catch (Exception ex)
        {
          System.Diagnostics.Debug.WriteLine(ex);
          throw new InvalidOperationException("Cannot obtain Log.Instance. " + ex.Message);
        }
      }
    }

    private Log()
    {
    }
    public void Err(string message)
    {
      if (!string.IsNullOrEmpty(message))
        _logger.Log(LogLevel.Error, message);
    }
    public void Err(string message, params object[] args)
    {
      if (!string.IsNullOrEmpty(message))
        _logger.Log(LogLevel.Error, message, args);
    }

    public void Err(Exception ex)
    {
      if (ex != null)
        _logger.Log(LogLevel.Error, ex.ToString);
    }

    public void Warn(string message, params object[] args)
    {
      if (!string.IsNullOrEmpty(message))
        _logger.Log(LogLevel.Warn, message, args);
    }
    public void Warn(string message)
    {
      if (!string.IsNullOrEmpty(message))
        _logger.Log(LogLevel.Warn, message);
    }
    public void Warn(Exception ex)
    {
      if (ex != null)
        _logger.Log(LogLevel.Warn, ex.ToString);
    }

    public void Info(string message)
    {
      if (!string.IsNullOrEmpty(message))
        _logger.Log(LogLevel.Info, message);
    }

    public void Info(string message, params object[] args)
    {
      if (!string.IsNullOrEmpty(message))
        _logger.Log(LogLevel.Info, message, args);
    }

    public void Info(Exception ex)
    {
      if (ex != null)
        _logger.Log(LogLevel.Info, ex.ToString);
    }

    public void Debug(string message)
    {
      if (!string.IsNullOrEmpty(message))
        _logger.Log(LogLevel.Debug, message);
    }

    public void Debug(string message, params object[] args)
    {
      if (!string.IsNullOrEmpty(message))
        _logger.Log(LogLevel.Debug, message, args);
    }

    public void Debug(Exception ex)
    {
      if (ex != null)
        _logger.Log(LogLevel.Debug, ex.ToString());
    }

  }
}
