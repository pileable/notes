﻿using System;

namespace Tariel.Notes.Services
{
  public interface ISyncService
  {
    void Sync();
    bool IsInProcess { get; }

    event EventHandler SyncStarted;
    event EventHandler SyncCompleted;

  }
}
