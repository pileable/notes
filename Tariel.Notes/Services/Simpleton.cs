﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using Tariel.Notes.Model;

namespace Tariel.Notes.Services
{
  public class Simpleton : ISyncService
  {
    private readonly ILocalRepository _localRepository;
    private readonly IRemoteRepository _remoteRepository;
    private bool _isInProcess;

    public Simpleton()
    {
      _localRepository = ServiceLocator.Current.GetAllInstances<ILocalRepository>().FirstOrDefault();
      if (_localRepository == null)
        throw new InvalidOperationException("ServiceLocator does not contain any ILocalRepository object.");
      _remoteRepository = ServiceLocator.Current.GetAllInstances<IRemoteRepository>().FirstOrDefault();
      if (_remoteRepository == null)
        throw new InvalidOperationException("ServiceLocator does not contain any IRemoteRepository object.");
      _isInProcess = false;
    }

    private IDictionary<Guid, Note> LoadRemoteRepository()
    {
      IDictionary<Guid, Note> ret = null;
      try
      {
        ret = _remoteRepository.Load();
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
      return ret;
    }

    private void SaveToRemoteRepository()
    {
      try
      {
        _remoteRepository.Save(_localRepository.Notes);
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
    }

    public void Sync()
    {
      try
      {
        OnSyncStarted(this, null);
        _isInProcess = true;
        StartSynchronisation();
        OnSyncCompleted(this, null);
      }
      catch (Exception e)
      {
        Log.Instance.Err(e);
      }
      finally
      {
        _isInProcess = false;
      }
    }

    private void StartSynchronisation()
    {
      Log.Instance.Info("Synchronization started");
      DateTime start = DateTime.UtcNow;

      var localNotes = _localRepository.Notes;
      var remoteNotes = LoadRemoteRepository();

      SyncLog latestLog = _localRepository.GetLatestSuccessfullSyncLog();
      if (latestLog == null)
      {
        foreach (var remoteNote in remoteNotes)
        {
          _localRepository.Add(remoteNote.Value);
        }
      }
      else
      {
        foreach (Note note in localNotes)
        {
          Note remote;
          if (remoteNotes.TryGetValue(note.Id, out remote))
          {
            if (remote.Modified > note.Modified)
            {
              note.Modified = remote.Modified;
              note.Header = remote.Header;
              note.Txt = remote.Txt;
              _localRepository.Update(note);
            }
            remoteNotes.Remove(note.Id);
          }
          else
          {
            if (latestLog.Finished.HasValue && note.Modified < latestLog.Finished.Value)
            {
              _localRepository.Delete(note);
            }
          }
        }
        foreach (var remoteNote in remoteNotes)
        {
          if (latestLog.Finished.HasValue)
          {
            if (remoteNote.Value.Modified >= latestLog.Finished.Value)
              _localRepository.Add(remoteNote.Value);
          }
         }
      }
      
      _localRepository.SaveChanges();
      SaveToRemoteRepository();

      latestLog = new SyncLog();
      latestLog.Started = start;
      latestLog.Finished = DateTime.UtcNow;

      _localRepository.Add(latestLog);
      _localRepository.SaveChanges();
      Log.Instance.Info("Synchronization finished");
    }

    public bool IsInProcess
    {
      get { return _isInProcess; }
    }
    public event EventHandler SyncStarted;
    public event EventHandler SyncCompleted;

    private void OnSyncStarted(object sender, ModelEventArgs e)
    {
      if (SyncStarted != null)
        SyncStarted(sender, e);
    }

    private void OnSyncCompleted(object sender, ModelEventArgs e)
    {
      if (SyncCompleted != null)
        SyncCompleted(sender, e);
    }
  }
}
