﻿using System.Data.Common;

namespace Tariel.Notes.Services
{
  public interface IDatabaseConnectionFactory<TContext>
  {
    DbConnection CreateConnection();
  }
}
