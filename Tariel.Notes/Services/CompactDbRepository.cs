using System;
using System.Data.Entity;
using System.Linq;
using Tariel.Notes.Model;

namespace Tariel.Notes.Services
{
  public class CompactDbRepository : ILocalRepository
  {
    private readonly NotesDataContext _context;
    private readonly object _syncObject = new object();

    public CompactDbRepository(NotesDataContext context)
    {
      if (context == null) throw new ArgumentNullException(nameof(context));
      _context = context;
    }

    public IOrderedQueryable<Note> Notes
    {
      get
      {
        lock (_syncObject)
        {
          try
          {
            return from n in _context.Notes orderby n.Modified descending select n;
          }
          catch (Exception e)
          {
            Log.Instance.Err(e);
          }
          return null;
        }
      }
    }

    public void Add(Note note)
    {
      if (note == null) throw new ArgumentNullException(nameof(note));
      lock (_syncObject)
      {
        try
        {
          if (!string.IsNullOrEmpty(note.Header))
          {
            if (note.Id == default(Guid))
              note.Id = Guid.NewGuid();
            //note.Modified = DateTime.UtcNow;
            _context.Notes.Add(note);
            OnEntryAdded(this, new ModelEventArgs(note));
          }
        }
        catch (Exception e)
        {
          Log.Instance.Err(e);
        }
      }
    }

    public void Add(SyncLog syncLog)
    {
      if (syncLog == null) throw new ArgumentNullException(nameof(syncLog));
      lock (_syncObject)
      {
        try
        {
          _context.SyncLogs.Add(syncLog);
        }
        catch (Exception e)
        {
          Log.Instance.Err(e);
        }
      }
    }

    public void Update(SyncLog syncLog)
    {
      if (syncLog == null) throw new ArgumentNullException(nameof(syncLog));
      lock (_syncObject)
      {
        try
        {
          var oldLog = _context.SyncLogs.Find(syncLog.Id);
          if (oldLog != null)
          {
            oldLog.Finished = syncLog.Finished;
          }
          else
          {
            _context.Entry(_context.SyncLogs.Attach(syncLog)).State = EntityState.Modified;
          }
        }
        catch (Exception e)
        {
          Log.Instance.Err(e);
        }
      }
      
    }

    public SyncLog GetLatestSuccessfullSyncLog()
    {
      lock (_syncObject)
      {
        try
        {
          var log =
            (from l in _context.SyncLogs where l.Finished.HasValue orderby l.Started descending select l).FirstOrDefault();
          return log;
        }
        catch (Exception e)
        {
          Log.Instance.Err(e);
        }
        return null;
      }
    }

    public void Update(Note note)
    {
      if (note == null) throw new ArgumentNullException(nameof(note));
      lock (_syncObject)
      {
        try
        {
          if (!string.IsNullOrEmpty(note.Header))
          {
            if (!string.IsNullOrEmpty(note.Header))
            {
              var oldNote = _context.Notes.Find(note.Id);
              if (oldNote != null)
              {
                oldNote.Header = note.Header;
                oldNote.Txt = note.Txt;
                oldNote.Modified = DateTime.UtcNow;
                OnEntryUpdated(this, new ModelEventArgs(note));
              }
              else
              {
                _context.Entry(_context.Notes.Attach(note)).State = EntityState.Modified;
              }
            }
          }
        }
        catch (Exception e)
        {
          Log.Instance.Err(e);
        }
      }
    }

    public void Delete(Note note)
    {
      if (note == null) throw new ArgumentNullException(nameof(note));
      lock (_syncObject)
      {
        try
        {
          _context.Notes.Remove(note);
          //_context.Entry(_context.Notes.Attach(note)).State = EntityState.Deleted;
          OnEntryDeleted(this, new ModelEventArgs(note));
        }
        catch (Exception e)
        {
          Log.Instance.Err(e);
        }
      }
    }

    public event EventHandler<ModelEventArgs> EntryAdded;
    public event EventHandler<ModelEventArgs> EntryUpdated;
    public event EventHandler<ModelEventArgs> EntryDeleted;

    private void OnEntryAdded(object sender, ModelEventArgs e)
    {
      if (EntryAdded != null)
        EntryAdded(sender, e);
    }

    private void OnEntryUpdated(object sender, ModelEventArgs e)
    {
      if (EntryUpdated != null)
        EntryUpdated(sender, e);
    }

    private void OnEntryDeleted(object sender, ModelEventArgs e)
    {
      if (EntryDeleted != null)
        EntryDeleted(sender, e);
    }

    public void SaveChanges()
    {
      lock (_syncObject)
      {
        try
        {
          _context.SaveChanges();
        }
        catch (Exception e)
        {
          Log.Instance.Err(e);
        }
      }
    }
  }
}
