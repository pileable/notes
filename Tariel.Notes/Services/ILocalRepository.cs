using System;
using System.Linq;
using Tariel.Notes.Model;

namespace Tariel.Notes.Services
{
  public interface ILocalRepository
  {
    void Add(Note note);
    void Update(Note note);
    void Delete(Note note);
    void SaveChanges();
    SyncLog GetLatestSuccessfullSyncLog();
    void Update(SyncLog syncLog);
    void Add(SyncLog syncLog);

    IOrderedQueryable<Note> Notes { get; }

    event EventHandler<ModelEventArgs> EntryAdded;
    event EventHandler<ModelEventArgs> EntryUpdated;
    event EventHandler<ModelEventArgs> EntryDeleted;
  }
}