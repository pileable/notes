﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tariel.Notes.Model;

namespace Tariel.Notes.Services
{
  public interface IRemoteRepository
  {
    IDictionary<Guid, Note> Load();
    void Save(IOrderedQueryable<Note> data);
  }
}
