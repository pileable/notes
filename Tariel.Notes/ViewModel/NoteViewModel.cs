﻿using System;
using System.ComponentModel;
using Tariel.Notes.Model;
using Tariel.Notes.Services;

namespace Tariel.Notes.ViewModel
{
  public class NoteViewModel : INotifyPropertyChanged
  {
    public NoteViewModel(Note model)
    {
      if (model == null) throw new ArgumentNullException(nameof(model));
      Model = model;
      Model.PropertyChanged += ModelOnPropertyChanged;
    }

    public Note Model { get; }

    public string Header
    {
      get { return Model.Header; }
      set
      {
        Model.Header = value;
        OnPropertyChanged("Header");
      }
    }

    public string Txt
    {
      get { return Model.Txt; }
      set
      {
        Model.Txt = value;
        OnPropertyChanged("Txt");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    private void ModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      try
      {
        OnPropertyChanged(e.PropertyName);
      }
      catch (Exception ex)
      {
        Log.Instance.Err(ex);
      }
    }

    protected virtual void OnPropertyChanged(string propertyName)
    {
      var handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
